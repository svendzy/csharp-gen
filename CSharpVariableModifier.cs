﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpVariableModifier
    {
        Default = 0,
		Private = 1,
		Public = 2,
		Protected = 4,
		Internal = 8,
		Const = 16,
		Static = 32,
        ReadOnly = 64
    }
}
