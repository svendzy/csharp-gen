﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpClassPropertyModifier
    {
        Default = 0,
        Public = 1,
        Protected = 2,
        Internal = 4,
        Private = 8,
        Static = 16,
        Virtual = 32,
        Override = 64,
        Abstract = 128,
        New = 256,
        Sealed = 512
    }
}
