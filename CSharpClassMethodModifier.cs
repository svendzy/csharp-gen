﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpClassMethodModifier
    {
        Default = 0,
        Public = 1,
        Protected = 2,
        Internal = 4,
        Private = 8,
        Static = 16,
        Virtual = 32,
        Sealed = 64,
        Override = 128,
        Abstract = 256
    }
}
