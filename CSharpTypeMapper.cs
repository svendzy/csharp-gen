﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public static class CSharpTypeMapper
    {
        private static Hashtable mapTable;

        static CSharpTypeMapper()
        {
            mapTable = new Hashtable();
            mapTable.Add("Object", "object");
            mapTable.Add("String", "string");
            mapTable.Add("Boolean", "bool");
            mapTable.Add("Byte", "byte");
            mapTable.Add("SByte", "sbyte");
            mapTable.Add("Int16", "short");
            mapTable.Add("UInt16", "ushort");
            mapTable.Add("Int32", "int");
            mapTable.Add("UInt32", "uint");
            mapTable.Add("Int64", "long");
            mapTable.Add("UInt64", "ulong");
            mapTable.Add("Single", "float");
            mapTable.Add("Double", "double");
            mapTable.Add("Decimal", "decimal");
            mapTable.Add("Char", "char");
        }

        public static string MapToFriendlyType(Type type)
        {
            if (mapTable.Contains(type.Name))
                return Convert.ToString(mapTable[type.Name]);
            else
                return type.Name;
        }
    }
}
