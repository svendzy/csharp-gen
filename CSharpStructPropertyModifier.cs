﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpStructPropertyModifier
    {
        Default = 0,
        Public = 1,
        Internal = 2,
        Private = 4,
        Static = 8,
        Override = 16
    }
}
