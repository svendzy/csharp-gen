﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpEnum : ICSharpCodeElement
    {
        private string _modifier;
        private string _enumName;
        private CSharpEnumType _enumType;
        private List<CSharpEnumValue> _enumValues;

        public CSharpEnum(CSharpEnumModifier modifier, string enumName)
            : this(modifier, enumName, CSharpEnumType.Default)
        {
        }

        public CSharpEnum(CSharpEnumModifier modifier, string enumName, CSharpEnumType enumType)
        {
            this._modifier = BuildModifier(modifier);
            this._enumName = enumName;
            this._enumType = enumType;
            this._enumValues = new List<CSharpEnumValue>();
        }

        public void AddValue(CSharpEnumValue val)
        {
            this._enumValues.Add(val);
        }

        private static string BuildModifier(CSharpEnumModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpEnumModifier.Default:
                    break;
                case CSharpEnumModifier.Private:
                    result = "private";
                    break;
                case CSharpEnumModifier.Protected:
                    result = "protected";
                    break;
                case CSharpEnumModifier.Internal:
                    result = "internal";
                    break;
                case CSharpEnumModifier.Public:
                    result = "public";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(string modifier, string enumName, CSharpEnumType enumType, List<CSharpEnumValue> _enumValues)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(modifier);
            sb.Append(" enum ");
            sb.Append(enumName);
            sb.AppendLine((enumType == CSharpEnumType.Default) ? String.Empty : " : " + enumType.ToString().ToLowerInvariant());
            sb.AppendLine("{");

            for (int i = 0; i < _enumValues.Count; i++)
            {
                sb.Append(_enumValues[i].Key);
                if (_enumValues[i].Val != null)
                {
                    sb.Append(" = ");
                    sb.Append(_enumValues[i].Val);
                }
                if (i < _enumValues.Count - 1)
                    sb.AppendLine(",");
                else
                    sb.AppendLine();
            }
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _enumName, _enumType, _enumValues);
        }
    }
}
