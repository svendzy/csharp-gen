﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpStruct : ICSharpCodeElement
    {
        private string _modifier;
        private string _structName;
        private string[] _inherits;
        private List<ICSharpCodeElement> _elements;

        private class CSharpStructConstructor : ICSharpCodeElement
        {
            CSharpStructConstructorModifier _modifier;
            private string _structName;
            private CSharpArgument[] _arguments;
            private string _initializer;
            private CSharpLiteral _literal;

            public CSharpStructConstructor(CSharpStructConstructorModifier modifier, string structName, CSharpArgument[] arguments, string initializer, CSharpLiteral literal)
            {
                this._modifier = modifier;
                this._structName = structName;
                this._literal = literal;
                this._arguments = arguments;
                this._initializer = initializer;
            }

            public string ToSource()
            {
                StringBuilder sb = new StringBuilder();
                switch (_modifier)
                {
                    case CSharpStructConstructorModifier.Default:
                        break;
                    case CSharpStructConstructorModifier.Default | CSharpStructConstructorModifier.Static:
                        sb.Append("static ");
                        break;
                    case CSharpStructConstructorModifier.Private:
                        sb.Append("private ");
                        break;
                    case CSharpStructConstructorModifier.Internal:
                        sb.Append("internal ");
                        break;
                    case CSharpStructConstructorModifier.Public:
                        sb.Append("public ");
                        break;
                }
                sb.Append(_structName);
                sb.Append("(");
                if ((_arguments != null) && (_arguments.Length > 0))
                {
                    for (int i = 0; i < _arguments.Length; i++)
                    {
                        sb.Append(_arguments[i].ToSource());
                        if (i < _arguments.Length - 1)
                            sb.Append(", ");
                    }
                }
                sb.AppendLine(")");
                if (!String.IsNullOrEmpty(_initializer))
                {
                    sb.Append(" : ");
                    sb.AppendLine(_initializer);
                }
                sb.AppendLine("{");
                if (_literal != null)
                    sb.Append(_literal.ToSource());
                sb.AppendLine("}");
                return sb.ToString();
            }
        }

        public CSharpStruct(string structName, string[] inherits)
            : this(CSharpStructModifier.Default, structName, inherits)
        {
        }

        public CSharpStruct(CSharpStructModifier modifier, string structName, string[] inherits)
        {
            this._modifier = BuildModifier(modifier);
            this._structName = structName;
            this._inherits = inherits;
            this._elements = new List<ICSharpCodeElement>();
        }

        public void AddConstructor(CSharpStructConstructorModifier modifier, CSharpArgument[] arguments, string initializer, CSharpLiteral literal)
        {
            CSharpStructConstructor constructor = new CSharpStructConstructor(modifier, this._structName, arguments, initializer, literal);
            this._elements.Add(constructor);
        }

        public void AddVariable(CSharpVariable variable)
        {
            this._elements.Add(variable);
        }

        public void AddProperty(CSharpStructProperty property)
        {
            this._elements.Add(property);
        }

        public void AddMethod(CSharpStructMethod method)
        {
            this._elements.Add(method);
        }

        private static string BuildModifier(CSharpStructModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpStructModifier.Default:
                    break;
                case CSharpStructModifier.Private:
                    result = "private";
                    break;
                case CSharpStructModifier.Protected:
                    result = "protected";
                    break;
                case CSharpStructModifier.Internal:
                    result = "internal";
                    break;
                case CSharpStructModifier.Public:
                    result = "public";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier,
            string structName,
            string[] inherits,
            List<ICSharpCodeElement> elements)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append("struct ");
            sb.Append(structName);
            if ((inherits != null) && (inherits.Length > 0))
            {
                sb.Append(" : ");
                sb.Append(String.Join(", ", inherits));
            }
            sb.AppendLine();
            sb.AppendLine("{");
            for (int i = 0; i < elements.Count; i++)
                sb.AppendLine(elements[i].ToSource());
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _structName, _inherits, _elements);
        }
    }
}
