﻿using System;
using System.Collections.Generic;
using System.Text;

enum testEnum
{
    test1 = 0,
    test2 = 1,
    test3 = 2
}

internal interface myInterface
{
    string property1 { get; set; }
    string property2 { get; }
    string property3 { set; }

    string getString();
    string getString2();
}

class myClass
{

}

struct csc
{
    static csc()
    {
    }

    private csc(int z)
    {
    }

    internal csc(int z, int z2, int z3)
    {
    }

    static int method1
    {
        get
        {
            return 10;
        }
    }
}



namespace CSharp_gen
{
    internal enum testEnum2
    {
        test1 = 0,
        test2 = 1,
        test3 = 2
    }

    interface myInterface
    {
        string property1 { get; set; }
        string property2 { get; }
        string property3 { set; }

        string getString();
        string getString2();
    }

    public abstract class myClass
    {
        int Property { get; set; }
        private int Property1 { get; set; }
        protected int Property2 { get; set; }
        internal int Property3 { get; set; }
        public int Property4 { get; set; }

        protected abstract int Property5 { get; set; }
        protected virtual int Property20 { get; set; }
        internal abstract int Property6 { get; set; }
        public abstract int Property7 { get; set; }

        static int Property8 { get; set; }
        private static int Property9 { get; set; }
        protected static int Property10 { get; set; }
        internal static int Property11 { get; set; }
        public static int Property12 { get; set; }
    };

    public class myClass2 : myClass
    {
        protected struct csc
        {
            static csc()
            {

            }

            csc(int x, int y)
            {
            }

            int getValue()
            {
                return 4;
            }
        }

        protected override int Property5
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        internal override int Property6
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Property7
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
