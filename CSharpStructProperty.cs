﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpStructProperty : ICSharpCodeElement
    {
        private string _modifier;
        private string _propertyName;
        private string _propertyType;
        private CSharpPropertyAccessor _accessor;
        private CSharpLiteral _literal;
        private bool _hasImplementation;

        public CSharpStructProperty(CSharpStructPropertyModifier modifier, string propertyName, string propertyType, CSharpPropertyAccessor accessor)
        {
            this._modifier = BuildModifier(modifier);
            this._propertyName = propertyName;
            this._propertyType = propertyType;
            this._accessor = accessor;
            this._hasImplementation = false;
        }

        public CSharpStructProperty(CSharpStructPropertyModifier modifier, string propertyName, string propertyType, CSharpLiteral literal)
        {
            this._modifier = BuildModifier(modifier);
            this._propertyName = propertyName;
            this._propertyType = propertyType;
            this._literal = literal;
            this._hasImplementation = true;
        }

        private static string BuildModifier(CSharpStructPropertyModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpStructPropertyModifier.Default:
                    break;
                case CSharpStructPropertyModifier.Default | CSharpStructPropertyModifier.Static:
                    result = "static";
                    break;
                case CSharpStructPropertyModifier.Private:
                    result = "private";
                    break;
                case CSharpStructPropertyModifier.Private | CSharpStructPropertyModifier.Static:
                    result = "private static";
                    break;
                case CSharpStructPropertyModifier.Internal:
                    result = "internal";
                    break;
                case CSharpStructPropertyModifier.Internal | CSharpStructPropertyModifier.Override:
                    result = "internal override";
                    break;
                case CSharpStructPropertyModifier.Internal | CSharpStructPropertyModifier.Static:
                    result = "internal static";
                    break;
                case CSharpStructPropertyModifier.Public:
                    result = "public";
                    break;
                case CSharpStructPropertyModifier.Public | CSharpStructPropertyModifier.Override:
                    result = "public override";
                    break;
                case CSharpStructPropertyModifier.Public | CSharpStructPropertyModifier.Static:
                    result = "public static";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier,
            string propertyName,
            string propertyType,
            CSharpPropertyAccessor accessor,
            CSharpLiteral literal,
            bool hasImplementation)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(propertyType);
            sb.Append(" ");
            sb.Append(propertyName);
            if (!hasImplementation)
            {
                switch (accessor)
                {
                    case CSharpPropertyAccessor.Set: 
                        sb.Append(" { set; }");
                        break;
                    case CSharpPropertyAccessor.Get: 
                        sb.Append(" { get; }");
                        break;
                    case CSharpPropertyAccessor.Both: 
                        sb.Append(" { get; set; }");
                        break;
                }
            }
            else
            {
                sb.AppendLine();
                sb.AppendLine("{");
                if (literal != null)
                    sb.Append(literal.ToSource());
                sb.AppendLine("}");
            }
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _propertyName, _propertyType, _accessor, _literal, _hasImplementation);
        }
    }
}
