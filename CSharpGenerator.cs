﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CSharp_gen
{
    public class CSharpGenerator
    {
        private string[] _using;
        private string _namespace;
        private List<ICSharpCodeElement> _elements;

        public CSharpGenerator() 
            : this(null, null)
        {
        }

        public CSharpGenerator(string[] _using) 
            : this(_using, null)
        {
        }

        public CSharpGenerator(string[] _using, string _namespace)
        {
            this._using = _using;
            this._namespace = _namespace;
            this._elements = new List<ICSharpCodeElement>();
        }

        public void AddEnum(CSharpEnum _enum)
        {
            this._elements.Add(_enum);
        }

        public void AddStruct(CSharpStruct _struct)
        {
            this._elements.Add(_struct);
        }

        public void AddInterface(CSharpInterface _interface)
        {
            this._elements.Add(_interface);
        }

        public void AddClass(CSharpClass _class)
        {
            this._elements.Add(_class);
        }

        public void AddLiteral(CSharpLiteral _literal)
        {
            this._elements.Add(_literal);
        }

        private void WriteUsing(StringBuilder sb)
        {
            if (_using != null)
            {
                for (int i = 0; i < _using.Length; i++)
                {
                    if (!String.IsNullOrEmpty(_using[i]))
                    {
                        sb.AppendFormat("using {0};", _using[i]);
                        sb.AppendLine();
                    }
                }
            }
        }

        private void WriteStartNamespace(StringBuilder sb)
        {
            if (!String.IsNullOrEmpty(_namespace))
            {
                sb.AppendFormat("namespace {0}", _namespace);
                sb.AppendLine();
                sb.AppendLine("{");
            }
        }

        private void WriteEndNamespace(StringBuilder sb)
        {
            if (!String.IsNullOrEmpty(_namespace))
            {
                sb.AppendLine();
                sb.AppendLine("}");
            }
        }

        public void Generate(string filename)
        {
            StringBuilder sb = new StringBuilder();
            WriteUsing(sb);
            WriteStartNamespace(sb);
            for (int i = 0; i < _elements.Count; i++)
                sb.Append(_elements[i].ToSource());
            WriteEndNamespace(sb);
            Console.Write(sb.ToString());
            Console.ReadLine();
        }
    }
}
