﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpStructMethod : ICSharpCodeElement
    {
        private string _modifier;
        private string _methodName;
        private string _returnType;
        private CSharpArgument[] _arguments;
        private CSharpLiteral _literal;

        public CSharpStructMethod(string methodName, string returnType, CSharpLiteral literal)
            : this(CSharpStructMethodModifier.Default, methodName, null, returnType, literal)
        {
        }

        public CSharpStructMethod(string methodName, CSharpArgument[] arguments, string returnType, CSharpLiteral literal)
            : this(CSharpStructMethodModifier.Default, methodName, arguments, returnType, literal)
        {
        }

        public CSharpStructMethod(CSharpStructMethodModifier modifier, string methodName, CSharpArgument[] arguments, string returnType, CSharpLiteral literal)
        {
            this._modifier = BuildModifier(modifier);
            this._methodName = methodName;
            this._returnType = returnType;
            this._arguments = arguments;
            this._literal = literal;
        }

        private static string BuildModifier(CSharpStructMethodModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpStructMethodModifier.Default:
                    break;
                case CSharpStructMethodModifier.Default | CSharpStructMethodModifier.Static:
                    result = "static";
                    break;
                case CSharpStructMethodModifier.Private:
                    result = "private";
                    break;
                case CSharpStructMethodModifier.Private | CSharpStructMethodModifier.Static:
                    result = "private static";
                    break;
                case CSharpStructMethodModifier.Internal:
                    result = "internal";
                    break;
                case CSharpStructMethodModifier.Internal | CSharpStructMethodModifier.Override:
                    result = "internal override";
                    break;
                case CSharpStructMethodModifier.Internal | CSharpStructMethodModifier.Static:
                    result = "internal static";
                    break;
                case CSharpStructMethodModifier.Public:
                    result = "public";
                    break;
                case CSharpStructMethodModifier.Public | CSharpStructMethodModifier.Override:
                    result = "public override";
                    break;
                case CSharpStructMethodModifier.Public | CSharpStructMethodModifier.Static:
                    result = "public static";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier,
            string methodName,
            string returnType,
            CSharpArgument[] arguments,
            CSharpLiteral literal)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(returnType);
            sb.Append(" ");
            sb.Append(methodName);
            sb.Append("(");
            if ((arguments != null) && (arguments.Length > 0))
            {
                for (int i = 0; i < arguments.Length; i++)
                {
                    sb.Append(arguments[i].ToSource());
                    if (i < arguments.Length - 1)
                        sb.Append(", ");
                }
            }
            sb.AppendLine(")");
            sb.AppendLine("{");
            if (literal != null)
                sb.Append(literal.ToSource());
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _methodName, _returnType, _arguments, _literal);
        }
    }
}
