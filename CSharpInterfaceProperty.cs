﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpInterfaceProperty : ICSharpCodeElement
    {
        private string _propertyName;
        private string _propertyType;
        private CSharpPropertyAccessor _accessor;

        public CSharpInterfaceProperty(string propertyName, string propertyType)
            : this(propertyName, propertyType, CSharpPropertyAccessor.Both)
        {
        }

        public CSharpInterfaceProperty(string propertyName, string propertyType, CSharpPropertyAccessor accessor)
        {
            this._propertyName = propertyName;
            this._propertyType = propertyType;
            this._accessor = accessor;
        }

        private static string GenerateCode(
            string propertyName,
            string propertyType,
            CSharpPropertyAccessor accessor)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(propertyType);
            sb.Append(" ");
            sb.Append(propertyName);
            switch (accessor)
            {
                case CSharpPropertyAccessor.Set: sb.Append(" { set; }");
                    break;
                case CSharpPropertyAccessor.Get: sb.Append(" { get; }");
                    break;
                case CSharpPropertyAccessor.Both: sb.Append(" { get; set; }");
                    break;
            }
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_propertyName, _propertyType, _accessor);
        }
    }
}
