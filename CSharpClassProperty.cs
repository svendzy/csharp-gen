﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpClassProperty : ICSharpCodeElement
    {
        private string _modifier;
        private string _propertyName;
        private string _propertyType;
        private CSharpPropertyAccessor _accessor;
        private CSharpLiteral _literal;
        private bool _hasImplementation;

        public CSharpClassProperty(CSharpClassPropertyModifier modifier, string propertyName, string propertyType, CSharpPropertyAccessor accessor)
        {
            this._modifier = BuildModifier(modifier);
            this._propertyName = propertyName;
            this._propertyType = propertyType;
            this._accessor = accessor;
            this._hasImplementation = false;
        }

        public CSharpClassProperty(CSharpClassPropertyModifier modifier, string propertyName, string propertyType, CSharpLiteral literal)
        {
            this._modifier = BuildModifier(modifier);
            this._propertyName = propertyName;
            this._propertyType = propertyType;
            this._literal = literal;
            this._hasImplementation = true;
        }

        private static string BuildModifier(CSharpClassPropertyModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpClassPropertyModifier.Default:
                    break;
                case CSharpClassPropertyModifier.Default | CSharpClassPropertyModifier.Static:
                    result = "static";
                    break;
                case CSharpClassPropertyModifier.Private:
                    result = "private";
                    break;
                case CSharpClassPropertyModifier.Private | CSharpClassPropertyModifier.Static:
                    result = "private static";
                    break;
                case CSharpClassPropertyModifier.Protected:
                    result = "protected";
                    break;
                case CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Static:
                    result = "protected static";
                    break;
                case CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Abstract:
                    result = "protected abstract";
                    break;
                case CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Virtual:
                    result = "protected virtual";
                    break;
                case CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Override:
                    result = "protected override";
                    break;
                case CSharpClassPropertyModifier.Internal:
                    result = "internal";
                    break;
                case CSharpClassPropertyModifier.Internal | CSharpClassPropertyModifier.Static:
                    result = "internal static";
                    break;
                case CSharpClassPropertyModifier.Internal | CSharpClassPropertyModifier.Abstract:
                    result = "internal abstract";
                    break;
                case CSharpClassPropertyModifier.Internal | CSharpClassPropertyModifier.Virtual:
                    result = "internal virtual";
                    break;
                case CSharpClassPropertyModifier.Internal | CSharpClassPropertyModifier.Override:
                    result = "internal override";
                    break;
                case CSharpClassPropertyModifier.Public:
                    result = "public";
                    break;
                case CSharpClassPropertyModifier.Public | CSharpClassPropertyModifier.Static:
                    result = "public static";
                    break;
                case CSharpClassPropertyModifier.Public | CSharpClassPropertyModifier.Abstract:
                    result = "public abstract";
                    break;
                case CSharpClassPropertyModifier.Public | CSharpClassPropertyModifier.Virtual:
                    result = "public virtual";
                    break;
                case CSharpClassPropertyModifier.Public | CSharpClassPropertyModifier.Override:
                    result = "public override";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier,
            string propertyName,
            string propertyType,
            CSharpPropertyAccessor accessor,
            CSharpLiteral literal,
            bool hasImplementation)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(propertyType);
            sb.Append(" ");
            sb.Append(propertyName);
            if (!hasImplementation)
            {
                switch (accessor)
                {
                    case CSharpPropertyAccessor.Set: 
                        sb.Append(" { set; }");
                        break;
                    case CSharpPropertyAccessor.Get: 
                        sb.Append(" { get; }");
                        break;
                    case CSharpPropertyAccessor.Both: 
                        sb.Append(" { get; set; }");
                        break;
                }
            }
            else
            {
                sb.AppendLine();
                sb.AppendLine("{");
                if (literal != null)
                    sb.Append(literal.ToSource());
                sb.AppendLine("}");
            }
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _propertyName, _propertyType, _accessor, _literal, _hasImplementation);
        }
    }
}
