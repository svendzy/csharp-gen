﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpEnumModifier
    {
        Default = 0,
        Private = 1,
        Protected = 2,
        Public = 4,
        Internal = 8
    }
}
