﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpStructModifier
    {
        Default = 0,
        Private = 1,
        Protected = 2,
        Internal = 4,
        Public = 8,
        New = 16
    }
}
