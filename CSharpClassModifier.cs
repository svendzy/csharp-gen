﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpClassModifier
    {
        Default = 0,
        Private = 1,
        Public = 2,
        Protected = 4,
        Internal = 8,
        Abstract = 16,
        Sealed = 32,
        Static = 64
    }
}
