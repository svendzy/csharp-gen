﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpInterfaceModifier
    {
        Default,
		Public,
		Internal
    }
}
