﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpArgument : ICSharpCodeElement
    {
        private CSharpArgumentModifier _modifier;
        private string _argName;
        private string _argType;

        public CSharpArgument(string argName, string argType)
            : this(CSharpArgumentModifier.Default, argName, argType)
        {
        }

        public CSharpArgument(CSharpArgumentModifier modifier, string argName, string argType)
        {
            this._modifier = modifier;
            this._argName = argName;
            this._argType = argType;
        }

        public string ToSource()
        {
            StringBuilder sb = new StringBuilder();
            switch (_modifier)
            {
                case CSharpArgumentModifier.Default:
                    sb.AppendFormat("{0} {1}", _argType, _argName);
                    break;
                case CSharpArgumentModifier.Out:
                    sb.AppendFormat("out {0} {1}", _argType, _argName);
                    break;
                case CSharpArgumentModifier.Ref:
                    sb.AppendFormat("ref {0} {1}", _argType, _argName);
                    break;
            }
            return sb.ToString();
        }
    }
}
