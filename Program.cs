﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CSharp_gen
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] _using = new string[] { "System", "System.Diagnostics", "System.net" };
            string _namespace = "Snoopsoft.Namespace";
           
            // Test CSharpEnum
            CSharpEnum _enum = new CSharpEnum(CSharpEnumModifier.Public, "testEnum");
            _enum.AddValue(new CSharpEnumValue("testvalue"));
            _enum.AddValue(new CSharpEnumValue("testvalue2", "2"));
            _enum.AddValue(new CSharpEnumValue("testvalue3", "4"));
            _enum.AddValue(new CSharpEnumValue("testvalue4"));

            // Test CSharpInterface
            CSharpInterface _interface = new CSharpInterface("IMyInterface");
            _interface.AddProperty(new CSharpInterfaceProperty("MyString","string", CSharpPropertyAccessor.Both));
            CSharpInterfaceMethod _method = new CSharpInterfaceMethod("testMethod", 
                new CSharpArgument[] 
                {
                    new CSharpArgument(CSharpArgumentModifier.Ref, "arg1","bool"),
                    new CSharpArgument(CSharpArgumentModifier.Default, "arg2", "string"),
                    new CSharpArgument(CSharpArgumentModifier.Out, "arg3", "float")
                }, "int");
            _interface.AddMethod(_method);
            _interface.AddLiteral(
                new CSharpLiteral(new string[] {"int myNumber { get; set; }"}));

            // Test CSharpClass
            CSharpClass _class = new CSharpClass(CSharpClassModifier.Public, "myClass", new string[] { "IView", "IBeatable" });
            _class.AddVariable(new CSharpVariable(CSharpVariableModifier.Private, "int", "variable1"));
            _class.AddVariable(new CSharpVariable(CSharpVariableModifier.Private, "string", "variable2"));
            _class.AddConstructor(CSharpClassConstructorModifier.Static, new CSharpLiteral("// Constructor code"));

            _class.AddMethod(new CSharpClassMethod(CSharpClassMethodModifier.Protected, "MYmETHOD",
                new CSharpArgument[] {new CSharpArgument(CSharpArgumentModifier.Ref, "arg1","string")},
                "int", new CSharpLiteral("Dette er en test")));
            _class.AddProperty(new CSharpClassProperty(CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Virtual, "Prop1", "string", CSharpPropertyAccessor.Set));
            _class.AddProperty(new CSharpClassProperty(CSharpClassPropertyModifier.Protected | CSharpClassPropertyModifier.Override, "Prop1", "string", 
                new CSharpLiteral("Test")));
            _class.AddDestructor(new CSharpLiteral("// Destructor code"));


            // Test CSharpStruct

            CSharpStruct _struct = new CSharpStruct("struct100", new string[] {"IStruct1", "IStruct2"});
            _struct.AddVariable(new CSharpVariable(CSharpVariableModifier.Default, "int", "myInt"));
            _struct.AddConstructor(CSharpStructConstructorModifier.Default, null, null, null);
            _struct.AddProperty(
                new CSharpStructProperty(CSharpStructPropertyModifier.Default | CSharpStructPropertyModifier.Static, "testProp", "int", null));
            _struct.AddMethod(
                new CSharpStructMethod(CSharpStructMethodModifier.Public | CSharpStructMethodModifier.Static,
                "method", new CSharpArgument[] {new CSharpArgument(CSharpArgumentModifier.Default, "testArg", "int")}, "string", null));

            CSharpGenerator gen = new CSharpGenerator(_using, _namespace);
            gen.AddEnum(_enum);
            gen.AddInterface(_interface);
            gen.AddClass(_class);
            gen.AddStruct(_struct);

            gen.Generate(Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.DesktopDirectory), "testcode.cs"));
        }
    }
}
