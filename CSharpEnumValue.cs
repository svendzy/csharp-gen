﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpEnumValue
    {
        private readonly string _key;
        private readonly string _val;

        public CSharpEnumValue(string key)
        {
            this._key = key;
            this._val = null;
        }

        public CSharpEnumValue(string key, string val)
        {
            this._key = key;
            this._val = val;
        }

        public string Key
        {
            get { return _key; }
        }

        public string Val
        {
            get { return _val; }
        }
    }
}
