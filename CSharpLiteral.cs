﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CSharp_gen
{
    public class CSharpLiteral : ICSharpCodeElement
    {
        private string _source;
        private Dictionary<string, string> _variables;

        public CSharpLiteral(string source)
        {
            if (source == null)
                throw new ArgumentException("Invalid value", "source");
            this._source = source + Environment.NewLine;
            this._variables = new Dictionary<string, string>();
        }

        public CSharpLiteral(string[] source)
        {
            if (source == null)
                throw new ArgumentException("Invalid value", "source");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < source.Length; i++)
                sb.AppendLine(source[i]);
            this._source = sb.ToString();
            this._variables = new Dictionary<string, string>();
        }

        public void Assign(string variableName, string variableValue)
        {
            if (String.IsNullOrEmpty(variableName))
                throw new ArgumentException("Invalid value", "variableName");
            if (variableValue == null)
                throw new ArgumentException("Invalid value", "variableValue");
            _variables.Add(variableName, variableValue);
        }

        public static CSharpLiteral FromFile(string sourceFile)
        {
            CSharpLiteral literal = null;
            using (StreamReader reader = new StreamReader(sourceFile))
            {
                StringBuilder sb = new StringBuilder();
                string sourceLine;
                while ((sourceLine = reader.ReadLine()) != null)
                {
                    sb.AppendLine(sourceLine);
                }
                literal = new CSharpLiteral(sb.ToString());
            }
            return literal;
        }

        public string ToSource()
        {
            StringBuilder sb = new StringBuilder(_source);
            foreach (KeyValuePair<string, string> kv in _variables)
                sb = sb.Replace("{" + kv.Key + "}", kv.Value);
            return sb.ToString();
        }
    }
}
