﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpVariable : ICSharpCodeElement
    {
        private string _modifier;
        private string _variableType;
        private string _variableName;
        private string _initializer;

        public CSharpVariable(string variableType, string variableName)
            : this(CSharpVariableModifier.Default, variableType, variableName, null)
        {
        }

        public CSharpVariable(string variableType, string variableName, string initializer)
            : this(CSharpVariableModifier.Default, variableType, variableName, initializer)
        {
        }

        public CSharpVariable(CSharpVariableModifier modifier, string variableType, string variableName)
            : this(modifier, variableType, variableName, null)
        {
        }

        public CSharpVariable(CSharpVariableModifier modifier, string variableType, string variableName, string initializer)
        {
            _modifier = BuildModifier(modifier);
            _variableType = variableType;
            _variableName = variableName;
            if (String.IsNullOrEmpty(initializer))
                _initializer = String.Empty;
            else
                _initializer = String.Format(" = {0}", initializer);
        }

        private static string BuildModifier(CSharpVariableModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpVariableModifier.Default:
                    break;
                case CSharpVariableModifier.Default | CSharpVariableModifier.Static:
                    result = "static";
                    break;
                case CSharpVariableModifier.Default | CSharpVariableModifier.ReadOnly:
                    result = "readonly";
                    break;
                case CSharpVariableModifier.Default | CSharpVariableModifier.Static | CSharpVariableModifier.ReadOnly:
                    result = "static readonly";
                    break;
                case CSharpVariableModifier.Default | CSharpVariableModifier.Const:
                    result = "const";
                    break;
                case CSharpVariableModifier.Private:
                    result = "private";
                    break;
                case CSharpVariableModifier.Private | CSharpVariableModifier.Static:
                    result = "private static";
                    break;
                case CSharpVariableModifier.Private | CSharpVariableModifier.ReadOnly:
                    result = "private readonly";
                    break;
                case CSharpVariableModifier.Private | CSharpVariableModifier.Static | CSharpVariableModifier.ReadOnly:
                    result = "private static readonly";
                    break;
                case CSharpVariableModifier.Private | CSharpVariableModifier.Const:
                    result = "private const";
                    break;
                case CSharpVariableModifier.Protected:
                    result = "protected";
                    break;
                case CSharpVariableModifier.Protected | CSharpVariableModifier.Static:
                    result = "protected static";
                    break;
                case CSharpVariableModifier.Protected | CSharpVariableModifier.ReadOnly:
                    result = "protected readonly";
                    break;
                case CSharpVariableModifier.Protected | CSharpVariableModifier.Static | CSharpVariableModifier.ReadOnly:
                    result = "protected static readonly";
                    break;
                case CSharpVariableModifier.Protected | CSharpVariableModifier.Const:
                    result = "protected const";
                    break;
                case CSharpVariableModifier.Public:
                    result = "public";
                    break;
                case CSharpVariableModifier.Public | CSharpVariableModifier.Static:
                    result = "public static";
                    break;
                case CSharpVariableModifier.Public | CSharpVariableModifier.ReadOnly:
                    result = "public readonly";
                    break;
                case CSharpVariableModifier.Public | CSharpVariableModifier.Static | CSharpVariableModifier.ReadOnly:
                    result = "public static readonly";
                    break;
                case CSharpVariableModifier.Public | CSharpVariableModifier.Const:
                    result = "public const";
                    break;
                case CSharpVariableModifier.Internal:
                    result = "internal";
                    break;
                case CSharpVariableModifier.Internal | CSharpVariableModifier.Static:
                    result = "internal static";
                    break;
                case CSharpVariableModifier.Internal | CSharpVariableModifier.ReadOnly:
                    result = "internal readonly";
                    break;
                case CSharpVariableModifier.Internal | CSharpVariableModifier.Static | CSharpVariableModifier.ReadOnly:
                    result = "internal static readonly";
                    break;
                case CSharpVariableModifier.Internal | CSharpVariableModifier.Const:
                    result = "internal const";
                    break;
                default:
                    throw new NotSupportedException();
            }
            return result;
        }

        private static string GenerateCode(string modifier, string variableType, string variableName, string initializer)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(variableType);
            sb.Append(" ");
            sb.Append(variableName);
            if (!String.IsNullOrEmpty(initializer))
            {
                sb.Append(initializer);
            }
            sb.Append(";");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _variableType, _variableName, _initializer);
        }
    }
}
