﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpClass : ICSharpCodeElement
    {
        private string _modifier;
        private string _className;
        private string[] _inherits;
        private List<ICSharpCodeElement> _elements;

        private class CSharpClassConstructor : ICSharpCodeElement
        {
            CSharpClassConstructorModifier _modifier;
            private string _className;
            private CSharpArgument[] _arguments;
            private string _initializer;
            private CSharpLiteral _literal;

            public CSharpClassConstructor(CSharpClassConstructorModifier modifier, string className, CSharpLiteral literal)
                : this(modifier, className, null, null, literal)
            {
            }

            public CSharpClassConstructor(CSharpClassConstructorModifier modifier, string className, CSharpArgument[] arguments, CSharpLiteral literal)
                : this(modifier, className, arguments, null, literal)
            {
            }

            public CSharpClassConstructor(CSharpClassConstructorModifier modifier, string className, CSharpArgument[] arguments, string initializer, CSharpLiteral literal)
            {
                this._modifier = modifier;
                this._className = className;
                this._literal = literal;
                this._arguments = arguments;
                this._initializer = initializer;
            }

            public string ToSource()
            {
                StringBuilder sb = new StringBuilder();
                switch (_modifier)
                {
                    case CSharpClassConstructorModifier.Default:
                        break;
                    case CSharpClassConstructorModifier.Private:
                        sb.Append("private ");
                        break;
                    case CSharpClassConstructorModifier.Protected:
                        sb.Append("protected ");
                        break;
                    case CSharpClassConstructorModifier.Internal:
                        sb.Append("internal ");
                        break;
                    case CSharpClassConstructorModifier.Public:
                        sb.Append("public ");
                        break;
                    case CSharpClassConstructorModifier.Static:
                        sb.Append("static ");
                        break;
                }
                sb.Append(_className);
                sb.Append("(");
                if ((_arguments != null) && (_arguments.Length > 0))
                {
                    for (int i = 0; i < _arguments.Length; i++)
                    {
                        sb.Append(_arguments[i].ToSource());
                        if (i < _arguments.Length - 1) 
                            sb.Append(", ");
                    }
                }
                sb.AppendLine(")");
                if (!String.IsNullOrEmpty(_initializer))
                {
                    sb.Append(" : ");
                    sb.AppendLine(_initializer);
                }
                sb.AppendLine("{");
                if (_literal != null)
                    sb.Append(_literal.ToSource());
                sb.AppendLine("}");
                return sb.ToString();
            }
        }

        private class CSharpClassDestructor : ICSharpCodeElement
        {
            private string _className;
            private CSharpLiteral _literal;

            public CSharpClassDestructor(string className, CSharpLiteral literal)
            {
                this._className = className;
                this._literal = literal;
            }

            public string ToSource()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("~");
                sb.Append(_className);
                sb.AppendLine("()");
                sb.AppendLine("{");
                sb.Append(_literal.ToSource());
                sb.AppendLine("}");
                return sb.ToString();
            }
        }

        public CSharpClass(string className)
            : this(CSharpClassModifier.Default, className, null)
        {
        }

        public CSharpClass(string className, string[] inherits)
            : this(CSharpClassModifier.Default, className, inherits)
        {
        }

        public CSharpClass(CSharpClassModifier modifier, string className, string[] inherits)
        {
            this._modifier = BuildModifier(modifier);
            if (String.IsNullOrEmpty(className))
                throw new ArgumentException("Invalid value", "className");
            this._className = className;
            this._inherits = inherits;
            this._elements = new List<ICSharpCodeElement>();
        }

        public void AddConstructor(CSharpClassConstructorModifier modifier, CSharpLiteral literal)
        {
            CSharpClassConstructor constructor = new CSharpClassConstructor(modifier, this._className, literal);
            this._elements.Add(constructor);
        }

        public void AddConstructor(CSharpClassConstructorModifier modifier, CSharpArgument[] arguments, CSharpLiteral literal)
        {
            CSharpClassConstructor constructor = new CSharpClassConstructor(modifier, this._className, arguments, literal);
            this._elements.Add(constructor);
        }

        public void AddConstructor(CSharpClassConstructorModifier modifier, CSharpArgument[] arguments, string initializer, CSharpLiteral literal)
        {
            CSharpClassConstructor constructor = new CSharpClassConstructor(modifier, this._className, arguments, initializer, literal);
            this._elements.Add(constructor);
        }

        public void AddVariable(CSharpVariable variable)
        {
            this._elements.Add(variable);
        }

        public void AddProperty(CSharpClassProperty property)
        {
            this._elements.Add(property);
        }

        public void AddMethod(CSharpClassMethod method)
        {
            this._elements.Add(method);
        }

        public void AddLiteral(CSharpLiteral literal)
        {
            this._elements.Add(literal);
        }

        public void AddDestructor(CSharpLiteral literal)
        {
            CSharpClassDestructor destructor = new CSharpClassDestructor(this._className, literal);
            this._elements.Add(destructor);
        }

        private static string BuildModifier(CSharpClassModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpClassModifier.Default:
                    break;
                case CSharpClassModifier.Default | CSharpClassModifier.Abstract:
                    result = "abstract";
                    break;
                case CSharpClassModifier.Default | CSharpClassModifier.Sealed:
                    result = "sealed";
                    break;
                case CSharpClassModifier.Default | CSharpClassModifier.Static:
                    result = "static";
                    break;
                case CSharpClassModifier.Private:
                    result = "private";
                    break;
                case CSharpClassModifier.Private | CSharpClassModifier.Abstract:
                    result = "private abstract";
                    break;
                case CSharpClassModifier.Private | CSharpClassModifier.Sealed:
                    result = "private sealed";
                    break;
                case CSharpClassModifier.Private | CSharpClassModifier.Static:
                    result = "private static";
                    break;
                case CSharpClassModifier.Protected:
                    result = "protected";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Abstract:
                    result = "protected abstract";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Sealed:
                    result = "protected sealed";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Static:
                    result = "protected static";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Internal:
                    result = "protected internal";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Internal | CSharpClassModifier.Abstract:
                    result = "protected internal abstract";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Internal | CSharpClassModifier.Sealed:
                    result = "protected internal sealed";
                    break;
                case CSharpClassModifier.Protected | CSharpClassModifier.Internal | CSharpClassModifier.Static:
                    result = "protected internal static";
                    break;
                case CSharpClassModifier.Public:
                    result = "public";
                    break;
                case CSharpClassModifier.Public | CSharpClassModifier.Abstract:
                    result = "public abstract";
                    break;
                case CSharpClassModifier.Public | CSharpClassModifier.Sealed:
                    result = "public sealed";
                    break;
                case CSharpClassModifier.Public | CSharpClassModifier.Static:
                    result = "public static";
                    break;
                case CSharpClassModifier.Internal:
                    result = "internal";
                    break;
                case CSharpClassModifier.Internal | CSharpClassModifier.Abstract:
                    result = "internal abstract";
                    break;
                case CSharpClassModifier.Internal | CSharpClassModifier.Sealed:
                    result = "internal sealed";
                    break;
                case CSharpClassModifier.Internal | CSharpClassModifier.Static:
                    result = "internal static";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier, 
            string className, 
            string[] inherits,
            List<ICSharpCodeElement> elements)
        {  
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append("class " + className);
            if ((inherits != null) && (inherits.Length > 0))
            {
                sb.Append(" : ");
                sb.Append(String.Join(", ", inherits));
            }
            sb.AppendLine();
            sb.AppendLine("{");
            for (int i = 0; i < elements.Count; i++)
                sb.AppendLine(elements[i].ToSource());
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _className, _inherits, _elements);
        }
    }
}
