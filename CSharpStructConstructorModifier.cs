﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpStructConstructorModifier
    {
        Default = 0,
        Private = 1,
        Internal = 2,
        Public = 4,
        Static = 8
    }
}
