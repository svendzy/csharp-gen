﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public enum CSharpClassConstructorModifier
    {
        Default,
        Private,
        Protected,
        Internal,
        Public,
        Static
    }
}
