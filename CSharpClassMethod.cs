﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpClassMethod : ICSharpCodeElement
    {
        private string _modifier;
        private string _methodName;
        private string _returnType;
        private CSharpArgument[] _arguments;
        private CSharpLiteral _literal;

        public CSharpClassMethod(string methodName, string returnType)
            : this(CSharpClassMethodModifier.Default, methodName, null, returnType, null)
        {
        }

        public CSharpClassMethod(string methodName, string returnType, CSharpLiteral literal)
            : this(CSharpClassMethodModifier.Default, methodName, null, returnType, literal)
        {
        }

        public CSharpClassMethod(string methodName, CSharpArgument[] arguments, string returnType)
            : this(CSharpClassMethodModifier.Default, methodName, null, returnType, null)
        {
        }

        public CSharpClassMethod(CSharpClassMethodModifier modifier, string methodName, CSharpArgument[] arguments, string returnType, CSharpLiteral literal)
        {
            this._modifier = BuildModifier(modifier);
            this._methodName = methodName;
            this._returnType = returnType;
            this._arguments = arguments;
            this._literal = literal;
        }

        private static string BuildModifier(CSharpClassMethodModifier modifier)
        {
            string result = String.Empty;
            switch (modifier)
            {
                case CSharpClassMethodModifier.Default:
                    break;
                case CSharpClassMethodModifier.Default | CSharpClassMethodModifier.Static:
                    result = "static";
                    break;
                case CSharpClassMethodModifier.Private:
                    result = "private";
                    break;
                case CSharpClassMethodModifier.Private | CSharpClassMethodModifier.Static:
                    result = "private static";
                    break;
                case CSharpClassMethodModifier.Protected:
                    result = "protected";
                    break;
                case CSharpClassMethodModifier.Protected | CSharpClassMethodModifier.Static:
                    result = "protected static";
                    break;
                case CSharpClassMethodModifier.Protected | CSharpClassMethodModifier.Virtual:
                    result = "protected virtual";
                    break;
                case CSharpClassMethodModifier.Protected | CSharpClassMethodModifier.Abstract:
                    result = "protected abstract";
                    break;
                case CSharpClassMethodModifier.Protected | CSharpClassMethodModifier.Override:
                    result = "protected override";
                    break;
                case CSharpClassMethodModifier.Internal:
                    result = "internal";
                    break;
                case CSharpClassMethodModifier.Internal | CSharpClassMethodModifier.Static:
                    result = "internal static";
                    break;
                case CSharpClassMethodModifier.Internal | CSharpClassMethodModifier.Virtual:
                    result = "internal virtual";
                    break;
                case CSharpClassMethodModifier.Internal | CSharpClassMethodModifier.Abstract:
                    result = "internal abstract";
                    break;
                case CSharpClassMethodModifier.Internal | CSharpClassMethodModifier.Override:
                    result = "internal override";
                    break;
                case CSharpClassMethodModifier.Public:
                    result = "public";
                    break;
                case CSharpClassMethodModifier.Public | CSharpClassMethodModifier.Static:
                    result = "public static";
                    break;
                case CSharpClassMethodModifier.Public | CSharpClassMethodModifier.Virtual:
                    result = "public virtual";
                    break;
                case CSharpClassMethodModifier.Public | CSharpClassMethodModifier.Abstract:
                    result = "public abstract";
                    break;
                case CSharpClassMethodModifier.Public | CSharpClassMethodModifier.Override:
                    result = "public override";
                    break;
                default:
                    throw new ArgumentException("Invalid value", "modifier");
            }
            return result;
        }

        private static string GenerateCode(
            string modifier,
            string methodName,
            string returnType,
            CSharpArgument[] arguments,
            CSharpLiteral literal)
        {
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrEmpty(modifier))
            {
                sb.Append(modifier);
                sb.Append(" ");
            }
            sb.Append(returnType);
            sb.Append(" ");
            sb.Append(methodName);
            sb.Append("(");
            if ((arguments != null) && (arguments.Length > 0))
            {
                for (int i = 0; i < arguments.Length; i++)
                {
                    sb.Append(arguments[i].ToSource());
                    if (i < arguments.Length - 1)
                        sb.Append(", ");
                }
            }
            if (literal != null)
            {
                sb.AppendLine(")");
                sb.AppendLine("{");
                sb.Append(literal.ToSource());
                sb.AppendLine("}");
            }
            else
                sb.Append(");");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _methodName, _returnType, _arguments, _literal);
        }
    }
}
