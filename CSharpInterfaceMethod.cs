﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpInterfaceMethod : ICSharpCodeElement
    {
        private string _methodName;
        private string _returnType;
        private CSharpArgument[] _arguments;

        public CSharpInterfaceMethod(string methodName, string returnType)
            : this(methodName, null, returnType)
        {
        }

        public CSharpInterfaceMethod(string methodName, CSharpArgument[] arguments, string returnType)
        {
            this._methodName = methodName;
            this._returnType = returnType;
            this._arguments = arguments;
        }

        private static string GenerateCode(
            string methodName,
            string returnType,
            CSharpArgument[] arguments)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(returnType);
            sb.Append(" ");
            sb.Append(methodName);
            sb.Append("(");
            if ((arguments != null) && (arguments.Length > 0))
            {
                for (int i = 0; i < arguments.Length; i++)
                {
                    sb.Append(arguments[i].ToSource());
                    if (i < arguments.Length - 1)
                        sb.Append(", ");
                }
            }
            sb.Append(");");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_methodName, _returnType, _arguments);
        }
    }
}
