# Library for generating C# source code #

For demonstrational purposes only

![csharp_gen.jpg](https://bitbucket.org/repo/RXMjXA/images/1348413481-csharp_gen.jpg)

## Functions: ##
* Generates C# source code by using a set of helper classes/utility-functions

Technologies used: Visual Studio 2010, C#, .Net Framework 2.0