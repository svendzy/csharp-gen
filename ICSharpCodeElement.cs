﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public interface ICSharpCodeElement
    {
        string ToSource();
    }
}
