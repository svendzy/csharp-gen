﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharp_gen
{
    public class CSharpInterface : ICSharpCodeElement
    {
        private CSharpInterfaceModifier _modifier;
        private string _interfaceName;
        private List<ICSharpCodeElement> _elements;
        private string[] _inherits;

        public CSharpInterface(string interfaceName)
            : this(CSharpInterfaceModifier.Default, interfaceName, null)
        {
        }

        public CSharpInterface(string interfaceName, string[] inherits)
            : this(CSharpInterfaceModifier.Default, interfaceName, inherits)
        {
        }

        public CSharpInterface(CSharpInterfaceModifier modifier, string interfaceName, string[] inherits)
        {
            this._modifier = modifier;
            this._interfaceName = interfaceName;
            this._elements = new List<ICSharpCodeElement>();
            this._inherits = inherits;
        }

        public void AddProperty(CSharpInterfaceProperty property)
        {
            this._elements.Add(property);
        }

        public void AddMethod(CSharpInterfaceMethod method)
        {
            this._elements.Add(method);
        }

        public void AddLiteral(CSharpLiteral literal)
        {
            this._elements.Add(literal);
        }

        private static string GenerateCode(
            CSharpInterfaceModifier modifier, 
            string interfaceName, 
            string[] inherits,
            List<ICSharpCodeElement> elements)
        {
            StringBuilder sb = new StringBuilder();
            switch (modifier)
            {
                case CSharpInterfaceModifier.Default: sb.Append("interface ");
                    break;
                case CSharpInterfaceModifier.Public: sb.Append("public interface ");
                    break;
                case CSharpInterfaceModifier.Internal: sb.Append("internal interface ");
                    break;
            }
            sb.Append(interfaceName);
            if ((inherits != null) && (inherits.Length > 0))
            {
                sb.Append(" : ");
                sb.Append(String.Join(",", inherits));
            }
            sb.AppendLine();
            sb.AppendLine("{");
            for (int i = 0; i < elements.Count; i++)
                sb.AppendLine(elements[i].ToSource());
            sb.AppendLine("}");
            return sb.ToString();
        }

        public string ToSource()
        {
            return GenerateCode(_modifier, _interfaceName, _inherits, _elements);
        }
    }
}
